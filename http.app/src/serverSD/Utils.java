package serverSD;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils {
	public static String IStoS (InputStream is) throws Exception {
		InputStreamReader isr =  new InputStreamReader(is,"utf-8");
		BufferedReader br = new BufferedReader(isr);

		int b;
		StringBuilder buf = new StringBuilder(512);
		while ((b = br.read()) != -1) {
			buf.append((char) b);
		}

		br.close();
		isr.close();
		
		return buf.toString();
	}
}
