package serverSD;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;

public class Cliente {
	private String url = "http://localhost";
	
	public Cliente (String url, int porta) {
		this.url += ":" + porta + url ;
	}
	
	public void put () throws Exception {
		URL url = new URL(this.url);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("PUT");

		// isso poderia ser passado como parametro ou lido do teclado
		Universidade nova = new Universidade ("UFAL");
		Universidade antiga = new Universidade ("ufal");
		
		Gson gson = new Gson();
		String[] msgArray = new String[]{antiga.toJSON(), nova.toJSON()};
		String jsonString = gson.toJsonTree(msgArray).toString();
		
		DataOutputStream wr = new DataOutputStream (httpCon.getOutputStream ());
		wr.writeBytes(jsonString);
		wr.flush();
		wr.close();
		httpCon.getInputStream();
	}

	public void delete () throws Exception {
		URL url = new URL(this.url);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("DELETE");

		// isso poderia ser passado como parametro ou lido do teclado
		int r = (int)(Math.random()*Main.ufs.size());
		Universidade uf = Main.ufs.get(r);

		DataOutputStream wr = new DataOutputStream (httpCon.getOutputStream ());
		wr.writeBytes(uf.toJSON());
		wr.flush();
		wr.close();
		httpCon.getInputStream();
	}

	public void get () throws Exception {
		URL url = new URL(this.url);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("GET");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		Gson gson = new Gson();
		String[] jsonArray = gson.fromJson(response.toString(), String[].class);
		System.out.println("Universidades cadastradas:");
		for (int i = 0; i < jsonArray.length; i++) {
			Universidade u = gson.fromJson(jsonArray[i], Universidade.class);
			System.out.println(" " + u.toString());
		}
	}

	public void post () throws Exception {
		URL url = new URL(this.url);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("POST");

		// isso poderia ser passado como parametro ou lido do teclado
		Universidade ufba = new Universidade ("ufba");

		DataOutputStream wr = new DataOutputStream (httpCon.getOutputStream ());
		wr.writeBytes(ufba.toJSON());
		wr.flush();
		wr.close();
		httpCon.getInputStream();
	}
}
