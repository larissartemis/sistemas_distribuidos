package serverSD;

import java.util.ArrayList;

public class Main {
	public static ArrayList<Universidade> ufs = new ArrayList<Universidade>(); 


	public static void main(String[] args) throws Exception {
		Universidade ufal = new Universidade("ufal");
		Universidade ufpe = new Universidade("ufpe");
		Universidade ufce = new Universidade("ufce");

		ufs.add(ufal);
		ufs.add(ufpe);
		ufs.add(ufce);

		String url = "/sd";
		int porta = 8000;

		new Servidor(url, porta);
		Cliente c = new Cliente(url, porta);

		c.post();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
		c.delete();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
		c.put();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
		c.get();

		System.exit(0);
	}
}
