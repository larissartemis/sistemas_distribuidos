package serverSD;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

class Gerenciador implements HttpHandler {
	@Override
	public void handle(HttpExchange t) throws IOException {
		String response = "";
		if (t.getRequestMethod().equals("GET")) {
			Gson gson = new Gson();
			int s = Main.ufs.size();
			String[] msgArray = new String[s];
			for (int i = 0; i < s; i++) {
				msgArray[i] = Main.ufs.get(i).toJSON();
			}
			response += gson.toJsonTree(msgArray).toString();

		} else if (t.getRequestMethod().equals("POST")) {
			String b = "";
			try {
				b = Utils.IStoS(t.getRequestBody());
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!b.isEmpty()) {
				Gson gsonUF = new Gson();
				Universidade u = gsonUF.fromJson(b, Universidade.class);
				Main.ufs.add(u);

				System.out.println("Universidade adicionada ~ " + u.toString());
			}
		} else if (t.getRequestMethod().equals("PUT")) {
			String b = "";
			try {
				b = Utils.IStoS(t.getRequestBody());
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!b.isEmpty()) {
				Gson gson = new Gson();
				String[] jsonArray = gson.fromJson(b, String[].class);
				ArrayList<Universidade> univ = new ArrayList<Universidade>();
				for (int i = 0; i < 2; i++) {
					Universidade u = gson.fromJson(jsonArray[i], Universidade.class);
					univ.add(u);
				}

				for (Universidade uf : Main.ufs) {
					if (uf.getNome().equals(univ.get(0).getNome())) {
						Main.ufs.remove(uf);
						Main.ufs.add(univ.get(1));

						System.out.println("Universidade atualizada de " + univ.get(0).getNome() 
								+ " para " + univ.get(1).getNome());

						break;
					}
				}
			}

		} else if (t.getRequestMethod().equals("DELETE")) {
			String b = "";
			try {
				b = Utils.IStoS(t.getRequestBody());
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!b.isEmpty()) {
				Gson gsonUF = new Gson();
				Universidade u = gsonUF.fromJson(b, Universidade.class);
				for (Universidade uf : Main.ufs) {
					if (uf.getNome().equals(u.getNome())) {
						Main.ufs.remove(uf);
						System.out.println("Universidade removida ~ " + u.toString());
						break;
					}
				}
			}
		}
		
		/*
		if (!response.isEmpty()) {
			System.out.println("Servidor: " + response);
		}
		
		System.out.println("\nStatus");
		for (Universidade uf : Main.ufs) {
			System.out.println(" " + uf.toString());
		}
		*/

		t.sendResponseHeaders(200, response.length());
		OutputStream os = t.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}
}
