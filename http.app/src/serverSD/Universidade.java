package serverSD;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Universidade {
	private String nome;
	
	public Universidade(String nome) {
		super();
		this.nome = nome;
	}
	
	public String toJSON () {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	} 
	
	@Override
	public String toString() {
		return "Nome: " + this.nome;
	}
}
