package serverSD;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Test {

	public static void main(String[] args) throws Exception {
		HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
		server.createContext("/test", new MyHandler());
		server.start();

		//		put();
		//		delete();
		//		get();
		post();
	}

	static class MyHandler implements HttpHandler {
		@Override
		public void handle(HttpExchange t) throws IOException {
			String response = "This is the response";
			if (t.getRequestMethod().equals("GET")) {
				response += " GET";
			} else if (t.getRequestMethod().equals("POST")) {
				response += " POST";
				
				InputStreamReader isr =  new InputStreamReader(t.getRequestBody(),"utf-8");
				BufferedReader br = new BufferedReader(isr);

				int b;
				StringBuilder buf = new StringBuilder(512);
				while ((b = br.read()) != -1) {
				    buf.append((char) b);
				}

				br.close();
				isr.close();
				
				System.out.println("data: " + buf.toString());
				
			} else if (t.getRequestMethod().equals("PUT")) {
				response += " PUT";
			} else if (t.getRequestMethod().equals("DELETE")) {
				response += " DELETE";
			}
			System.out.println(response);

			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

	public static void put () throws Exception {
		URL url = new URL("http://localhost:8000/test");
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("PUT");
		OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
		out.write("Resource content");
		out.close();
		httpCon.getInputStream();
	}

	public static void delete () throws Exception {
		URL url = new URL("http://localhost:8000/test");
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("DELETE");
		httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		httpCon.connect();
		OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
		out.write("Resource content");
		out.close();
		httpCon.getInputStream();
	}

	public static void get () throws Exception {
		URL obj = new URL("http://localhost:8000/test");
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("GET");
		httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		httpCon.connect();
		httpCon.getInputStream();
	}

	public static void post () throws Exception {
		URL obj = new URL("http://localhost:8000/test");
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestMethod("POST");

		httpCon.setRequestProperty("Content-Type", "application/json");
		String json = "{\"id\":1,\"nome\":\"ufal\"}"; // universidades.toJSON()

		DataOutputStream wr = new DataOutputStream (httpCon.getOutputStream ());
		wr.writeBytes(json);
		wr.flush();
		wr.close();

		httpCon.getInputStream();
	}
}