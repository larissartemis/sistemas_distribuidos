package serverSD;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

public class Servidor {
	public Servidor (String url, int porta) throws IOException {
		HttpServer server = HttpServer.create(new InetSocketAddress(porta), 0);
		server.createContext(url, new Gerenciador());
		server.start();
	}
}
